import { Injectable } from '@angular/core';

import { Title } from '@angular/platform-browser';

@Injectable()
export class DataService {
  /* texto = 'dinamico';
  otro;*/

  // trae la data del input desde home.component.ts
  nuevoInput: string;

  // para saber en que development environment esta, localhost o produccion.
  ambiente: string;

  // le paso al constructor Title para configurar el titulo
  constructor(private titleService: Title) { }

  // este metodo lo llaman por servicio y le pasan el titulo dinamicamente.
  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }

}
