import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import { environment } from 'environments/environment';
import { ResponseModel } from 'app/service/models/ResponseModel';
import { hotelModel } from 'app/service/models/hotelModel';

@Injectable()
export class HotelService {
  constructor(private http: Http) { }

  SetEstado(start: string, name?: string): Observable<boolean> {
    
    let headers: Headers = new Headers();
    let page = 'page=1&per_page=50';

    let parameters = '';

    if (start) {
      parameters = start + page;
    } else if (name) {
      parameters = name + page;
    } else {
      parameters = page;
    }
    
    return this.http.get(environment.apiUrl + 'hoteles?' + parameters)
        .map((response: Response) => {
            let result: ResponseModel = <ResponseModel>response.json() && response.json();
           
            if(result.data.length == 0 ) {
              throw new Error('Servidor no responde');
            }
            return result;
            
        })
        .catch(e => {
            if (e.status === 401) {                            
                return Observable.throw('Usuario no autorizado');
            }
            else if (e.status === 0){
                return Observable.throw('Servidor no responde');
            }
            else {                   
                if(e.message !== null)
                {                        
                    return Observable.throw(e.message);      
                }
                else{
                    return Observable.throw('Servidor devolvió un conflicto');      
                }                                      
            }
        }); 
    }
}
