import { amenitieModel } from "./amenitieModel";

export class hotelModel {
    public id: number;
    public name: string;
    public stars: number;
    public price: number;
    public image: string;
    public amenities: amenitieModel[];
}