import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SlideInOutAnimation } from '../animations';
import { SVGCacheService } from 'ng-inline-svg';

import { DataService } from '../data.service';
import { HotelService } from 'app/service/hotel.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [SlideInOutAnimation],  
  providers: [HotelService]
})
export class HomeComponent implements OnInit {
  // texto;
  dataInput;
  ambienteProd;
  animationStateA = 'in';
  animationStateB = 'in';
  animationStateM = 'in';
  stars = '';

  // para seleccionar un elemento si o si tiene que buscar en el html #resultados, con ViewChild lo selecciona
  // y le pasa un nombre de var resultadostRef del tipo ElementRef, abajo lo usa como por ejemplo
  // this.resultadostRef.nativeElement.style.display = 'none';
  @ViewChild('resultados') resultadostRef: ElementRef;

  // Texto si no encuentra nada en la busqueda
  noResponse;

  // setea en false el loading que importa en app.module.ts
  public loading = false;

  // url api
  private apiURL = 'http://localhost:4210/api/hoteles?' + this.stars + 'page=1&per_page=50';

  data: any = {};

  onSubmit() {
    // cuando hace clic toma el valor del input
    this.dataInput = (<HTMLInputElement>document.getElementsByName('texto')[0]).value;

    let resultStart: string;
    if (!this.dataInput) {
      resultStart = '';
    } else {
      resultStart = 'name=' + this.dataInput + '&';
    }

    this.hotelService.SetEstado('', resultStart)
      .subscribe(result =>{
          this.data = result;
        },
        err => {
          console.log('Mensaje de Error: ' + err);
          this.data = [];
          // una vez que trae la respuesta oculta el loading.
          this.loading = false;
          this.noResponse = true;
        });
    
    // lo que escriba el usuario en el input y envie, lo pasa a dataService
    this.dataService.nuevoInput = this.dataInput;
  }

  constructor(private dataService: DataService,
    // importa ActivatedRoute para saber el id que tiene la receta.
              private route: ActivatedRoute,
              // necesita Router para poder usar las rutas.
              private router: Router,
              private http: Http,
              svgService: SVGCacheService,
              private hotelService: HotelService) { }

  toggleShowDiv(divName: string) {
    if (divName === 'divA') {
      this.animationStateA = this.animationStateA === 'out' ? 'in' : 'out';
    }

    if (divName === 'divB') {
      this.animationStateB = this.animationStateB === 'out' ? 'in' : 'out';
    }

    if (divName === 'divM') {
      this.animationStateM = this.animationStateM === 'out' ? 'in' : 'out';
    }
  }

  ngOnInit() {
    this.dataService.setTitle('AlM - Inicio');

    // Cuando consulta a la api, le pasa true a loading, asi muestra el cargando de ngx-loading
    this.loading = true;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.get(this.apiURL)
      // .map((res: Response) => res.json())
      .map((res: Response) => res.json())
      .subscribe(
        data => {
          // una vez que trae la respuesta oculta el loading.
          this.loading = false;
          this.data = data;

          // muestra el div #resultados una vez que traiga info de la api
          this.resultadostRef.nativeElement.style.display = 'block';
        },
        err => {
          console.log('Mensaje de Error: ' + err);
          // una vez que trae la respuesta oculta el loading.
          this.loading = false;
          this.noResponse = true;
        }
    )    
  }

  countStars(number: number) {
    let starArray = Array(number);
    return starArray;
  }

  selectStars(number: number) {
    let resultStart: string;
    if (!number) {
      resultStart = '';
    } else {
      resultStart = 'starts=' + number + '&';
    }

    this.hotelService.SetEstado(resultStart)
      .subscribe(result =>{
          this.data = result;
        });
  }
}
