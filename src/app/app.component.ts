import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { DataService } from './data.service';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private dataService: DataService,
              // importa ActivatedRoute para saber el id que tiene la receta.
              private route: ActivatedRoute,
              // necesita Router para poder usar las rutas.
              private router: Router,
              private http: Http) {
  }




  ngOnInit() {
    // si esta en localhost le pasa a dataService localhost
    if (window.location.href.indexOf('localhost') !== -1) {
      this.dataService.ambiente = 'localhost';
    } else {
      // le pasa a dataService produccion
      this.dataService.ambiente = 'produccion';
    }

  }
}
