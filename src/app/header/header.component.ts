import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { DataService } from '../data.service';
// necesario para inyectar el titulo de forma dinamica.
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // otro = 'texto from header';
  title;

  constructor(private dataService: DataService,
    // importa ActivatedRoute para saber el id que tiene la receta.
              private route: ActivatedRoute,
              // necesita Router para poder usar las rutas.
              private router: Router,
              // lo mete en el constructor para poder usarlo apenas se ejecuta HeaderComponent
              meta: Meta, title: Title) {

                // title.setTitle('HI ' + this.dataService.texto);
              }

  ngOnInit() {
    /* this.dataService.otro = this.otro;
    this.title = this.dataService.texto; */
  }

}
