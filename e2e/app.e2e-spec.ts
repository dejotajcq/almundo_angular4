import { EmpleosimplePage } from './app.po';

describe('empleosimple App', () => {
  let page: EmpleosimplePage;

  beforeEach(() => {
    page = new EmpleosimplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
