# SPA Single Page Application Almundo.com

Ejercicio de Prueba de Selección. Este sitio se comunica con la API alojada en https://bitbucket.org/nicocatalogna/nodetestapirest

### Instalación

Requiere [Node.js](https://nodejs.org/) para correr.

Instalar las dependencias e iniciar el servidor.

```sh
$ git clone https://dejotajcq@bitbucket.org/dejotajcq/almundo_angular4.git
$ cd almundo_angular4
$ npm install
$ ng serve
```

Navegar a esta url http://localhost:4200/ para visualizar la pagina, y cada cambio que se haga en el codigo se actualiza automaticamente.

#### Compilar carpeta dist
For production release:
```sh
ng build --prod --aot
```